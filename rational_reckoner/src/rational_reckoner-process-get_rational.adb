with Rational_Numbers.IO;
separate(Rational_Reckoner.Process)
procedure Get_Rational(R: out Rational) is
begin
   loop
      begin
         IO.Get(R);
         exit;
      exception
         when Data_Error =>
            Skip_Line; New_Line;
            Put_Line("Not a rational, try again ");
            Put('#');
      end;
   end loop;
end Get_Rational;
