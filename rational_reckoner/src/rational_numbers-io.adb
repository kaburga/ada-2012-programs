with Ada.Text_IO, Ada.Integer_Text_IO;
use Ada;
with Rational_Numbers.Slave;

package body Rational_Numbers.IO is

   procedure Get(X: out Rational) is
      N: Integer;     -- numerator
      D: Integer;     -- denominator
      C: Character;
      EOL: Boolean;   -- end of line
   begin
      -- read the (possibly) signed numerator
      -- this also skips spaces and newlines
      Integer_Text_IO.Get(N);
      Text_IO.Look_Ahead(C, EOL);
      if EOL or else C /= '/' then
         raise Text_IO.Data_Error;
      end if;
      Text_IO.Get(C);   -- remove the / character
      Text_IO.Look_Ahead(C, EOL);
      if EOL or else C not in '0' .. '9' then
         raise Text_IO.Data_Error;
      end if;
      -- read the unsigned denominator
      Integer_Text_IO.Get(D);
      if D = 0 then
         raise Text_IO.Data_Error;
      end if;
      X := Slave.Normal((N, D));
   end Get;

   procedure Put(X: in Rational) is
   begin
      Integer_Text_IO.Put(X.Num, 0);
      Text_IO.Put('/');
      Integer_Text_IO.Put(X.Den, 0);
   end Put;

end Rational_Numbers.IO;
