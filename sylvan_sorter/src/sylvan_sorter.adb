with Lists, Read_List, Print_List;
with Trees, Print_Tree;
with Convert_List_To_Tree;
with Convert_Tree_To_List;
with Ada.Text_IO; use Ada.Text_IO;

procedure Sylvan_Sorter is
   The_List: Lists.List;
   The_Tree: Trees.Tree;
   Max_Value: Integer;
begin
   Put("Welcome to the Sylvan Sorter");
   New_Line(2);
   loop
      Put_Line("Enter list of positive integers ending with 0");
      Read_List(The_List, Max_Value);
      exit when Lists.Is_Empty(The_List);
      Print_List(The_List, Max_Value);
      Convert_List_To_Tree(The_List, The_Tree);
      Print_Tree(The_Tree, Max_Value);
      Convert_Tree_To_List(The_Tree, The_List);
      Print_List(The_List, Max_Value);
   end loop;
   New_Line;
   Put_Line("Finished"); Skip_Line(2);
end Sylvan_Sorter;
