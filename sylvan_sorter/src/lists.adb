package body Lists is
  
  function Is_Empty(L: List) return Boolean is
  begin
    return L = null;
  end Is_Empty;

  procedure Clear(L: out List) is
  begin
    L := null;
  end Clear;

  function Make_List(V: Integer) return List is
  begin
    return new Cell'(null, V);
  end Make_List;

  procedure Take_From_List(L: in out List; V: out Integer) is
  begin
    V := L.Value;
    L := List(L.Next);
  end Take_From_List;

  procedure Append(First: in out List; Second: in List) is
    Local: access Cell := First;
  begin
    if First = null then
      First := Second;
    else
      while Local.Next /= null loop
        Local := Local.Next;
      end loop;
      Local.Next := Second;
    end if;
  end Append;

end Lists;

