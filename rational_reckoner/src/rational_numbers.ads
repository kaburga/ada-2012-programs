package Rational_Numbers is
   type Rational is private;

   -- unary operators
   function "+" (X: Rational) return Rational;
   function "-" (X: Rational) return Rational;

   -- binary operators
   function "+" (X, Y: Rational) return Rational;
   function "-" (X, Y: Rational) return Rational;
   function "*" (X, Y: Rational) return Rational;
   function "/" (X, Y: Rational) return Rational;

   -- constructor and selector functions
   function "/" (X: Integer; Y: Positive) return Rational;
   function Numerator(R: Rational) return Integer;
   function Denominator(R: Rational) return Positive;

private
   type Rational is
      record
         Num: Integer  := 0;  -- numerator
         Den: Positive := 1; -- denominator
      end record;

end Rational_Numbers;
