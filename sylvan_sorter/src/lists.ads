package Lists is
   type List is private;

   function Is_Empty(L: List) return Boolean;
   procedure Clear(L: out List);
   function Make_List(V: Integer) return List;
   procedure Take_From_List(L: in out List;
                            V: out Integer);
   procedure Append(First: in out List;
                    Second: in List);
private
   type Cell is
      record
         Next: access Cell;
         Value: Integer;
      end record;

   type List is access all Cell;
end Lists;

