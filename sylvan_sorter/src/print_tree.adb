with Page;  with Num_Size;
with Trees; use Trees;
with Ada.Text_IO; use Ada;

procedure Print_Tree(T: in Tree; Max_Value: in Integer) is

   Max_Width: Integer := Num_Size(Max_Value) * 2**(Depth(T)-1);
   A: array(1..4*Depth(T)-3) of Page.Line := (others => (others => ' '));

   procedure Put(N, Row, Col, Width: Integer) is
      Size: constant Integer := Num_Size(N);
      Offset: constant Integer := (Width - Size + 1)/2;
      Number: constant Integer := N;
   begin
      if Size > Width then
         for I in 1 .. Integer'Max(Width, 1) loop
            A(Row)(Col+I-1) := '*';
         end loop;
      else
         A(Row)(Col+Offset..Col+Offset+Size-1) := Integer'Image(Number);
      end if;
   end Put;

   procedure Do_It(T: Tree; Row, Col, W: Integer) is
      Left: constant Tree := Left_Subtree(T);
      Right: constant Tree := Right_Subtree(T);
   begin
      Put(Node_Value(T), Row, Col, W);
      if not (Is_Empty(Left) and Is_Empty(Right)) then
         A(Row+1)(Col+W/2) := '|';
      end if;

      if not Is_Empty(Left) then
         A(Row+2)(Col+W/4 .. Col+W/2) := (others => '-');
         A(Row+3)(Col+W/4) := '|';
         Do_It(Left, Row+4, Col, W/2);
      end if;

      if not Is_Empty(Right) then
         A(Row+2)(Col+W/2 .. Col+3*W/4) := (others => '-');
         A(Row+3)(Col+3*W/4) := '|';
         Do_it(Right, Row+4, Col+W/2, (W+1)/2);
      end if;
   end Do_It;

begin
   if Max_Width > Page.Width then
      Max_Width := Page.Width;
   end if;
   Do_It(T, 1, 1, Max_Width);
   Text_IO.New_Line;
   for I in A'Range loop
      Text_IO.New_Line;
      Text_IO.Put(A(I));
   end loop;
   Text_IO.New_Line(2);
end Print_Tree;
