with Lists, Trees;  use Lists, Trees;

procedure Convert_Tree_To_List(T: in Tree; L: out List) is
   Right_L: List;
begin
   if Is_Empty(T) then Clear(L); return; end if;
   Convert_Tree_To_List(Left_Subtree(T),L);
   Append(L, Make_List(Node_Value(T)));
   Convert_Tree_To_List(Right_Subtree(T), Right_L);
   Append(L, Right_L);
end Convert_Tree_To_List;

