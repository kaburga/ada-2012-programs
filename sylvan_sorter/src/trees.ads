package Trees is
   type Tree is private;

   function Is_Empty(T: Tree) return Boolean;
   procedure Clear(T: out Tree);
   procedure Insert(T: in out Tree; V: in Integer);
   function Depth(T: Tree) return Integer;

   function Left_Subtree(T: Tree) return Tree;
   function Right_Subtree(T: Tree) return Tree;
   function Node_Value(T: Tree) return Integer;

private
   type Node;
   type Tree is access Node;
   type Node is
      record
         Left, Right: Tree;
         Value: Integer;
      end record;

end Trees;
