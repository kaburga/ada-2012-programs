with Rational_Numbers.Slave;

package body Rational_Numbers is
   use Slave;

   function "+" (X: Rational) return Rational is
   begin
      return X;
   end "+";

   function "-" (X: Rational) return Rational is
   begin
      return (-X.Num, X.Den);
   end "-";

   function "+" (X, Y: Rational) return Rational is
   begin
      return Normal((X.Num*Y.Den + Y.Num*X.Den, X.Den*Y.Den));
   end "+";

   function "-" (X, Y: Rational) return Rational is
   begin
      return Normal((X.Num*Y.Den - Y.Num*X.Den, X.Den*Y.Den));
   end "-";

   function "*" (X, Y: Rational) return Rational is
   begin
      return Normal((X.Num*Y.Num, X.Den*Y.Den));
   end "*";

   function "/" (X, Y: Rational) return Rational is
      N: Integer := X.Num*Y.Den;
      D: Integer := X.Den*Y.Num;
   begin
      if D < 0 then D := -D; N := -N; end if;
      return Normal((Num => N, Den => D));
   end "/";

   function "/" (X: Integer; Y: Positive) return Rational is
   begin
      return Normal((Num => X, Den => Y));
   end "/";

   function Numerator(R: Rational) return Integer is
   begin
      return R.Num;
   end Numerator;

   function Denominator(R: Rational) return Positive is
   begin
      return R.Den;
   end Denominator;

end Rational_Numbers;
