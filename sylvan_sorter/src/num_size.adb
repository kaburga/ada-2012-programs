function Num_Size(N: Integer) return Integer is
begin
   return Integer'Image(N)'Length;
end Num_Size;
