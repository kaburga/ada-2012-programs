with Lists, Trees;  use Lists, Trees;

procedure Convert_List_To_Tree(L: List; T: out Tree) is
   Temp: List := L;
   Value: Integer;
begin
   Clear(T);
   loop
      exit when Is_Empty(Temp);
      Take_From_List(Temp, Value);
      Insert(T, Value);
   end loop;
end Convert_List_To_Tree;

