with Lists; use Lists;
with Ada.Integer_Text_IO; use Ada;

procedure Read_List(L: out List; Max_Value: out Integer) is
   Value: Integer;
begin
   Max_Value := 0;
   Clear(L);
   loop
      Integer_Text_IO.Get(Value);
      exit when Value = 0;
      if Value > Max_Value then
         Max_Value := Value;
      end if;
      Append(L, Make_List(Value));
   end loop;
end Read_List;
