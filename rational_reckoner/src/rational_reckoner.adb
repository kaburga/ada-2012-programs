with Rat_Stack;
with Ada.Text_IO; use Ada.Text_IO;

procedure Rational_Reckoner is
   C: Character;
   Control_Error, Done: exception;

   procedure Process(C: Character) is separate;

begin
   Put("Welcome to the Rational Reckoner");
   New_Line(2);
   Put_Line("Operations are + - * / ? ! plus eXit");
   Put_Line("Input rational by #[sign]digits/digits");
   Rat_Stack.Clear;
   loop
      begin
         Get(C);
         Process(C);
      exception
         when Rat_Stack.Error =>
            New_Line;
            Put_Line("Stack overflow/underflow, stack reset");
            Rat_Stack.Clear;

         when Control_Error =>
            New_Line;
            Put_Line("Unexpected character, not # + - * / ? ! or X");

         when Done =>
            exit;
      end;
   end loop;
   New_Line;
   Put_Line("Finished");
   Skip_Line(2);
end Rational_Reckoner;
