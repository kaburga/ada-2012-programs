with Rational_Numbers;
use Rational_Numbers;

package Rat_Stack is
   Error: exception;
   procedure Clear;
   procedure Push(R: in Rational);
   function Pop return Rational;
end Rat_Stack;
