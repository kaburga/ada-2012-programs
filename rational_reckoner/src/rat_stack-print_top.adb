with Rational_Numbers.IO;
with Ada.Text_IO;

private with Rat_Stack.Data;

procedure Rat_Stack.Print_Top is
   use Data;
begin
   if Top = 0 then
      Ada.Text_IO.Put("Nothing on stack");
   else
      Rational_Numbers.IO.Put(Stack(Top));
   end if;
   Ada.Text_IO.New_Line;
end Rat_Stack.Print_Top;
