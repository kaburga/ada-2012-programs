with Page; with Num_Size;
with Lists; use Lists;
with Ada.Text_IO, Ada.Integer_Text_IO; use Ada;

procedure Print_List(L: in List; Max_Value: in Integer) is
   Temp: List := L;
   Value: Integer;
   Count: Integer := 0;
   Field: constant Integer := Num_Size(Max_Value);
begin
   Text_IO.New_Line;
   loop
      exit when Is_Empty(Temp);
      Take_From_List(Temp, Value);
      Count := Count + Field;
      if Count > Page.Width then
         Text_IO.New_Line;
         Count := Field;
      end if;
      Integer_Text_IO.Put(Value, Field);
   end loop;
   Text_IO.New_Line(2);
end Print_List;
