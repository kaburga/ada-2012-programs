package body Trees is

   function Is_Empty(T: Tree) return Boolean is
   begin
      return T = null;
   end Is_Empty;

   procedure Clear(T: out Tree) is
   begin
      T := null;
   end Clear;

   procedure Insert(T: in out Tree; V: in Integer) is
   begin
      if T = null then
         T := new Node'(null, null, V);
      elsif V < T.Value then
         Insert(T.Left, V);
      else
         Insert(T.Right, V);
      end if;
   end Insert;

   function Depth(T: Tree) return Integer is
   begin
      if T = null then return 0; end if;
      return 1 + Integer'Max(Depth(T.Left), Depth(T.Right));
   end Depth;

   function Left_Subtree(T: Tree) return Tree is
   begin
      return T.Left;
   end Left_Subtree;

   function Right_Subtree(T: Tree) return Tree is
   begin
      return T.Right;
   end Right_Subtree;

   function Node_Value(T: Tree) return Integer is
   begin
      return T.Value;
   end Node_Value;

end Trees;
