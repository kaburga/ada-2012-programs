with Rat_Stack.Print_Top; use Rat_Stack;
with Rational_Numbers; use Rational_Numbers;

separate(Rational_Reckoner)
procedure Process(C: Character) is
   R: Rational;
   procedure Get_Rational(R: out Rational) is separate;
begin
   case C is
      when '#' =>
         Get_Rational(R);
         Push(R);
      when '+' =>
         Push(Pop + Pop);
      when '-' =>
         R := Pop; Push(Pop - R);
      when '*' =>
         Push(Pop * Pop);
      when '/' =>
         R := Pop; Push(Pop / R);
      when '?' =>
         Print_Top;
      when '!' =>
         Print_Top; R := Pop;
      when ' ' =>
         null;
      when 'X' | 'x' =>
         raise Done;
      when others =>
         raise Control_Error;
   end case;
end Process;
