package Page is
   Width: constant Integer := 40;
   subtype Line is String(1 .. Width);
end Page;
